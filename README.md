# paas

![](https://img.shields.io/badge/written%20in-PHP%2C%20bash-blue)

A platform for running hosted applications.

`paas` provides a web interface allowing users to upload their applications and run them on the remote server. Users can manage multiple application projects, each with multiple software versions and multiple running instances. Users can invite other sub-user accounts. Applications see a dedicated filesystem via container isolation.

Users must package their applications into a .zip file, along with a `start` file containing necessary commands to launch the application (e.g. `pip install dependencies ; python3 app.py`). The base operating system image is customisable but by default includes Python 2, Python 3, PHP 5.6, perl and node.js along with pip and npm.

Tags: sysadmin

## Features

- Customisable base image from `debootstrap`
- Runs on the `systemd-nspawn` container engine (`docker`/`rkt` in future?)
- Live view application `stdout`/`stderr` in the web interface (line buffered via `socktailf`)
- User invite system
- Single-page app


## Download

- [⬇️ paas-1.0.0.tar.xz](dist-archive/paas-1.0.0.tar.xz) *(69.56 KiB)*
